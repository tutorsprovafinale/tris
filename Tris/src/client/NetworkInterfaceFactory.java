package client;

public class NetworkInterfaceFactory {

	private NetworkInterfaceFactory(){
		
	}
	public static NetworkInterface getInterface(String param){
		if(param.equals("1")) return new SocketInterface();
		else return new RMIInterface();
	}
}
