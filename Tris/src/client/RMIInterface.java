package client;


import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import server.rmi.RemoteTris;

public class RMIInterface implements NetworkInterface {

	private RemoteTris tris;
	
	public String connect() {
		String name = "RemoteTris";
        Registry registry;
		try {
			registry = LocateRegistry.getRegistry(2026);
		} catch (RemoteException e) {			
			e.printStackTrace();
			return "";
		}
        try {
			tris = (RemoteTris) registry.lookup(name);
		} catch (AccessException e) {
			e.printStackTrace();
			return "";
		} catch (RemoteException e) {
			e.printStackTrace();
			return "";
		} catch (NotBoundException e) {
			e.printStackTrace();
			return "";
		}
		try {
			return tris.connect();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	public boolean close() {
		return true;
	}

	@Override
	public String getMap() throws RemoteException {
		return tris.getMap();
	}

	@Override
	public String move(int i, int j, String player) throws RemoteException {
		return tris.move(i, j, player);
	}

	@Override
	public String isEnded() throws RemoteException {
		return tris.isEnded();
	}

	@Override
	public String getWinner() throws RemoteException {
		return tris.getWinner();
	}

	
}
