package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.RemoteException;



public class SocketInterface implements NetworkInterface {

	private Socket s;
	private PrintWriter pw;
	private BufferedReader br; 
	
	public SocketInterface() {
		
	} 
	
	@Override
	public String connect() throws IOException {
		try {
			s = new Socket("127.0.0.1", 8888);
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return "";
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
		
		try {
			pw = new PrintWriter(s.getOutputStream(), true);
		} catch (IOException e) {			
			e.printStackTrace();
			s.close();
			return "";
		}
		try {
			br = new BufferedReader(new InputStreamReader(s.getInputStream()));
		} catch (IOException e) {			
			e.printStackTrace();
			pw.close();
			s.close();
			return "";
		}
		pw.println("action=connect");
		try {
			return br.readLine();	
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	public boolean close() {
		try{
			br.close();
			pw.close();
			s.close();
			return true;
		} catch(IOException ex){
			return false;
		}
	}


	@Override
	public String getMap() throws RemoteException {
		pw.println("action=getMap");
		try {
			return br.readLine();	
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String move(int i, int j, String player) throws RemoteException {
		pw.println("action=move&x="+i+"&y="+j+"&player="+player);
		try {
			return br.readLine();	
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String isEnded() throws RemoteException {
		pw.println("action=isEnded");
		try {
			return br.readLine();	
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String getWinner() throws RemoteException {
		pw.println("action=getWinner");
		try {
			return br.readLine();	
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

}
