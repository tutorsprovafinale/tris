package client;

import java.io.IOException;
import java.rmi.RemoteException;

public interface NetworkInterface {
	String connect() throws IOException;
	boolean close() throws IOException;
	
	String getMap() throws RemoteException;
	String move(int i, int j, String player) throws RemoteException;
	
	String isEnded() throws RemoteException;
	
	String getWinner() throws RemoteException;
}
