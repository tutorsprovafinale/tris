package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;

public class Client {

	public static void main(String[] args) throws NotBoundException,
			IOException, InterruptedException {

		String read1 = "";
		while (!read1.equals("1") && !read1.equals("2")) {
			System.out.println("Choose your network interface:");
			System.out.println("1 - Socket");
			System.out.println("2 - RMI");
			read1 = readLine("\n");
			if (!read1.equals("1") && !read1.equals("2"))
				System.out.println("You typed the wrong command!");
		}

		NetworkInterface ni = NetworkInterfaceFactory.getInterface(read1);
		System.out.println("Connection to the room... Waiting for other player");
		String player = ni.connect();

		System.out.println("You are " + player);
		boolean finish = false;
		String toPrint;

		// while the game is online
		while (!finish) {
			
			// if it is not ended
			if (!ni.isEnded().equals("true")) {

				// print the map
				System.out.println(ni.getMap().replace(";", "\n"));
				
				// ask the user for a move
				String read = readLine("\nPress Q to exit or insert x,y to move\nMove: ");
				if (read.equals("Q")) {
					
					// if the move is quit, exit
					finish = true;
				} else {
					
					// parse the move
					String[] result=read.split(",");
					
					// make the move
					toPrint = ni.move(Integer.parseInt(result[0]), Integer.parseInt(result[1]), player);
					
					// analyze if win/lose/draw/nothing
					finish = analyze(toPrint);
				} 
			} else {
				
				// if ended check who won.
				finish=true;
				System.out.println("THE WINNER IS "+ni.getWinner());
			}
		}

	}
	
	/**
	 * Return true if the game is ended
	 * @param toPrint is the result of the move
	 * @return true if the game is ended
	 */
	private static boolean analyze(String toPrint) {
		boolean finish=false;
		if (toPrint.equals("YOULOSE")) {
			finish = true;
			System.out.println("KONGRATS, " + toPrint);
		} else if (toPrint.equals("YOUWIN")) {
			finish = true;
			System.out.println(toPrint);
		} else if (toPrint.equals("YOUDRAW")) {
			finish = true;
			System.out.println("YAY " + toPrint);
		} else {
			System.out.println("MSG " + toPrint);
		}
		return finish;
	}

	private static String readLine(String format, Object... args)
			throws IOException {
		if (System.console() != null) {
			return System.console().readLine(format, args);
		}
		System.out.print(String.format(format, args));

		BufferedReader br = null;
		InputStreamReader isr = null;
		String read = null;

		isr = new InputStreamReader(System.in);
		br = new BufferedReader(isr);
		read = br.readLine();

		return read;
	}
}