package server.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

public class SocketServer implements Runnable {

	private int port;
	private String address;
	private ServerSocket server;
	private boolean listening;
	private String status; 
	private List<SocketHandler> handlers;
	
	public SocketServer() {
		port = 8888;
		address = "127.0.0.1";
		listening = false;
		status = "Created";
		handlers = new LinkedList<SocketHandler>();
	}
	
	public SocketServer(int port, String address) {
		super();
		this.port = port;
		this.address = address;
		listening = false;
		status = "Created";
		handlers = new LinkedList<SocketHandler>();
	}
	
	public String getStatus() {
		return status;
	}

	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public void startListening() throws IOException {
		if(!listening){
			
			server = new ServerSocket(port);
			status = "Listening...";
			listening = true;
			while(listening){
				try{
					Socket s = server.accept();
					SocketHandler sh = new SocketHandler(s);
					handlers.add(sh);
					sh.start();
				} catch (IOException ex){ 
					ex.printStackTrace();
				}
			}
		}
	}
	
	public void endListening() throws IOException{
		if(listening){
			listening = false;
			for(SocketHandler sh : handlers)
				sh.Close();			
			
			server.close();
			status = "Closed.";
		}
	}

	public void run() {
		try{
			startListening();		
		} catch (IOException ex){
			status = "Error: "+ex.getMessage();
		}
	}
}
