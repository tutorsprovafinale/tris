package server.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class SocketHandler extends Thread{
	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;
	private boolean stop;

	public SocketHandler(Socket socket) {
		super();
		this.socket = socket;
		stop = false;
	}

	public Socket getSocket() {
		return socket;
	}

	public void run() {
		try{
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			String input, output;
			while((input = in.readLine()) != null && !stop){
				String[] splitted = input.split("&");
				Map<String, String> params = new HashMap<String, String>();
				for(String s : splitted){
					params.put(s.split("=")[0],s.split("=")[1]);
				}
				output = CommandHandler.handleCommand(params);
				out.println(output);
			}
		
		} catch (IOException ex){
			ex.printStackTrace();
		}
	}

	public void Close() throws IOException{
		stop = true;
		in.close();
		out.close();
		socket.close();
	}
	
	
}
