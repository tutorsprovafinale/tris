package server.socket;

import java.rmi.RemoteException;
import java.util.Map;

import server.Tris;

public class CommandHandler {
	
	

	private CommandHandler() {

	}

	public static String handleCommand(Map<String, String> param) throws RemoteException {
		String action = param.get("action");
		String x = param.get("x");
		String y = param.get("y");
		String player = param.get("player");
		if (action.equals("connect")) {
			return Tris.getInstance().connect();
		} else if (action.equals("getMap")) {
			return Tris.getInstance().getMap();
		} else if (action.equals("isEnded")) {
			return Tris.getInstance().isEnded();
		} else if (action.equals("getWinner")) {
			return Tris.getInstance().getWinner();
		} else if (x!= null && y!= null && player != null){
			return Tris.getInstance().move(Integer.parseInt(x), Integer.parseInt(y), player);
		} else
			return "WRONG CALL";
	}
}
