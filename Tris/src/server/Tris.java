package server;

import java.rmi.RemoteException;

import server.rmi.RemoteTris;

/**
 * The engine of a remote Tris game. It's a room where 2 player can join and
 * play one against the other.
 * 
 * @author andrea.sgarro
 *
 */
public class Tris implements RemoteTris {

	// Singleton pattern
	private static Tris instance;

	// variables to end the game
	private boolean youlose = false, youdraw = false;;
	private int countMoves = 0;
	private final int MAX_MOVES = 9;
	private String winner;

	// model of the game
	String map[][] = new String[3][3];
	int countPlayer;
	String turnOf;
	boolean first;

	/**
	 * Singleton pattern
	 * 
	 * @return the instance of the tris
	 */
	public static Tris getInstance() {
		if (instance == null)
			instance = new Tris();
		return instance;
	}

	/**
	 * Public constructor with initialization
	 */
	public Tris() {
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				map[i][j] = " ";

		turnOf = "X";
		countPlayer = 0;
		first = true;
	}

	/**
	 * The method perform a move in (x,y) by the player (player). If it is the
	 * turn of the other player the current one goes in wait.
	 * 
	 * @param x
	 *            row of the matrix
	 * @param y
	 *            column of the matrix
	 * @param player
	 *            identifier of the player (X or O)
	 * @return can return correct move, wrong move, you win, you lose, you draw.
	 */
	@Override
	public synchronized String move(int x, int y, String player)
			throws RemoteException {
		// if it is not my turn go in wait
		while (!turnOf.equals(player)) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		// check if the move is legal
		if (map[x][y].equals(" ") && x < 3 && y < 3 && x >= 0 && y >= 0) {
			
			// count moves for draw
			countMoves++;
			
			// make the move
			map[x][y] = player;
			
			// check if the current player has won
			boolean win = checkWin();
			
			// if not draw
			if (countMoves < MAX_MOVES) {
				
				// if none wins
				if (!win) {
					
					// turn of the opponent
					toggleTurnOf();
					
					// wake up him
					this.notifyAll();
					
					// go to sleep
					try {
						this.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					
					// the other loses
					youlose = true;
					winner = turnOf;
					this.notifyAll();
					return "YOUWIN";

				}
			} else {
				
				// it's a tie!
				youdraw = true;
				this.notifyAll();
				return "YOUDRAW";
			}
			return "Correct move, opponent turn";
		} else {
			return "Wrong move, still your turn";
		}
	}

	/**
	 * check if the current player has won
	 * @return true if won, false if not
	 */
	private boolean checkWin() {
		if (map[0][0].equals(turnOf) && map[0][1].equals(turnOf)
				&& map[0][2].equals(turnOf))
			return true;
		else if (map[1][0].equals(turnOf) && map[1][1].equals(turnOf)
				&& map[1][2].equals(turnOf))
			return true;
		else if (map[2][0].equals(turnOf) && map[2][1].equals(turnOf)
				&& map[2][2].equals(turnOf))
			return true;
		else if (map[0][0].equals(turnOf) && map[1][0].equals(turnOf)
				&& map[2][0].equals(turnOf))
			return true;
		else if (map[0][1].equals(turnOf) && map[1][1].equals(turnOf)
				&& map[2][1].equals(turnOf))
			return true;
		else if (map[0][2].equals(turnOf) && map[1][2].equals(turnOf)
				&& map[2][2].equals(turnOf))
			return true;
		else if (map[0][0].equals(turnOf) && map[1][1].equals(turnOf)
				&& map[2][2].equals(turnOf))
			return true;
		else if (map[2][0].equals(turnOf) && map[1][1].equals(turnOf)
				&& map[0][2].equals(turnOf))
			return true;
		else
			return false;
	}
	
	/**
	 * Change the current player
	 */
	private void toggleTurnOf() {
		if (turnOf.equals("X"))
			turnOf = "O";
		else
			turnOf = "X";
	}

	/**
	 * Return a string with all the map
	 */
	@Override
	public String getMap() throws RemoteException {
		String toRet = "";
		for (int i = 0; i < 3; i++) {
			toRet += " -  -  -;";
			for (int j = 0; j < 3; j++) {
				toRet += "|" + map[i][j] + "|";
			}
			toRet += ";";
		}
		toRet += " -  -  -;";
		return toRet;
	}

	/** 
	 * Start the connection with the game. If the number of player is exactly 2 the game starts
	 */
	@Override
	public synchronized String connect() {
		countPlayer++;
		while (countPlayer < 2) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		this.notify();
		if (first) {
			first = false;
			return "X";
		} else {
			try {
				this.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "O";
		}
	}

	/**
	 * Return true if the game is ended
	 */
	@Override
	public String isEnded() throws RemoteException {
		if (!youlose && !youdraw)
			return "false";
		else
			return "true";
	}

	/** 
	 * Return the identifier of the winner (X or O or DRAW)
	 */
	@Override
	public String getWinner() throws RemoteException {
		if (youlose)
			return winner;
		else
			return "DRAW";
	}
}