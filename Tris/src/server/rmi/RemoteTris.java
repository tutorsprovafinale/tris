package server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteTris extends Remote {
	
	public String move(int x,int y,String player) throws RemoteException ;
	
	public String getMap() throws RemoteException;
	
	public String connect() throws RemoteException;
	
	public String isEnded() throws RemoteException;
	
	public String getWinner() throws RemoteException;

}
