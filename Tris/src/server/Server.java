package server;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import server.rmi.RemoteTris;
import server.socket.SocketServer;

public class Server {
	public static void main(String[] args) throws IOException,
			NotBoundException {
		Registry registry = null;
		String name = "RemoteTris";
		// Starting RMI server

		try {

			RemoteTris tris = Tris.getInstance();
			RemoteTris stub = (RemoteTris) UnicastRemoteObject.exportObject(
					tris, 0);
			registry = LocateRegistry.createRegistry(2026);
			registry.bind(name, stub);

			System.out.println("ComputeEngine bound");
		} catch (Exception e) {
			System.err.println("ComputeEngine exception:");
			e.printStackTrace();
			registry = null;
		}

		// Starting socket server
		SocketServer server = new SocketServer();
		System.out.println("Starting the server...");
		Thread t = new Thread(server);
		t.start();
		System.out.println("Server started. Status: " + server.getStatus()
				+ ". Port: " + server.getPort());
		boolean finish = false;

		while (!finish) {
			String read = readLine("Press Q to exit\n");
			if (read.equals("Q")) {
				finish = true;
			}
		}
		
		t.interrupt();
		server.endListening();		
		if (registry != null)
			registry.unbind(name);
		System.exit(0);
		
	}

	private static String readLine(String format, Object... args)
			throws IOException {
		if (System.console() != null) {
			return System.console().readLine(format, args);
		}
		System.out.print(String.format(format, args));

		BufferedReader br = null;
		InputStreamReader isr = null;
		String read = null;

		isr = new InputStreamReader(System.in);
		br = new BufferedReader(isr);
		read = br.readLine();

		return read;
	}
}
